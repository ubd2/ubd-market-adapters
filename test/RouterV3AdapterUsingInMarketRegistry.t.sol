// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import "forge-std/console.sol";

// import {MarketAdapterUniswapV3} from "../src/MarketAdapterUniswapV3.sol";
import "../contracts/MarketAdapterUniswapV3.sol";
import "../interfaces/IMarketRegistry.sol";

// ETH Mainnet addresses
address constant WETH = 0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2;
address constant DAI = 0x6B175474E89094C44Da98b954EedeAC495271d0F;
address constant USDT = 0xdAC17F958D2ee523a2206206994597C13D831ec7;
address constant WBTC = 0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599;
address constant market = 0x1B4074355Bb5b3983dF638CB769bdC12d34Ac882;

address constant UNI_SWAP_ROUTERV3 = 0xE592427A0AEce92De3Edee1F18E0157C05861564;
address constant UNI_QUOTER = 0xb27308f9F90D607463bb33eA1BeBb41C27CE5AB6;
uint256 constant USDT_MINT_AMOUNT = 10_000e6;

struct Market {
        address marketAdapter;
        address oracleAdapter;
        uint256 slippage;
    }


interface IERC20Mint is IERC20 {
    function mint(uint256 amountMint) external;
    function issue(uint256 amountMint) external;
    function owner() external view returns (address);
}

contract UniV3TestWithMarketRegistry is Test {
    IERC20 private dai = IERC20(DAI);
    IERC20 private wbtc = IERC20(WBTC);
    IERC20Mint private usdt = IERC20Mint(USDT);

    MarketAdapterUniswapV3 private uniAdapter = new MarketAdapterUniswapV3(
    	'UniV3',
    	ISwapRouter(UNI_SWAP_ROUTERV3),
    	WETH,
        UNI_QUOTER
    );

    function setUp() public {}

    function test_DeployedAdress() public {
        console.log("MarketAdapterUniswapV3:", address(uniAdapter));
        assertEq(address(uniAdapter.SWAP_ROUTERV3()), UNI_SWAP_ROUTERV3);
    }

    function testWETH() public {
        (bool success, bytes memory r)  = address(UNI_SWAP_ROUTERV3).call(
            abi.encodeWithSignature(
                "WETH9()"
            )
        );
        console.log("weth:", success);
    }

    function test_usingAdapterV3ToMarket() public {
        //console.log(IMarketRegistry(market).UBD_TEAM_ADDRESS());
        IMarketRegistry.Market memory mrkt;

        vm.startPrank(0x71373aa15b6d537E70138A39709B50e32C3660Ec);
        
        mrkt.marketAdapter = address(uniAdapter);
        mrkt.oracleAdapter = address(uniAdapter);
        mrkt.slippage = 0;

        IMarketRegistry(market).setMarketParams(WETH, mrkt);
        IMarketRegistry(market).setMarketParams(USDT, mrkt);
        
        address[] memory path = new address[](2);
        path[0] = WETH;
        path[1] = USDT;

        uint256 amountOut;
        amountOut = IMarketRegistry(market).getAmountOut(1000000, path);
        assertEq(amountOut, 0);
    }
}
