// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import "forge-std/console.sol";

// import {MarketAdapterUniswapV3} from "../src/MarketAdapterUniswapV3.sol";
import "../contracts/MarketAdapterUniswapV3.sol";

// ETH Mainnet addresses
address constant WETH = 0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2;
address constant DAI = 0x6B175474E89094C44Da98b954EedeAC495271d0F;
address constant USDT = 0xdAC17F958D2ee523a2206206994597C13D831ec7;
address constant WBTC = 0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599;

address constant UNI_SWAP_ROUTERV3 = 0xE592427A0AEce92De3Edee1F18E0157C05861564;
address constant UNI_QUOTER = 0xb27308f9F90D607463bb33eA1BeBb41C27CE5AB6;
uint256 constant USDT_MINT_AMOUNT = 10_000e6;


interface IERC20Mint is IERC20 {
    function mint(uint256 amountMint) external;
    function issue(uint256 amountMint) external;
    function owner() external view returns (address);
}

contract UniV3TestETH is Test {
    IERC20 private dai = IERC20(DAI);
    IERC20 private wbtc = IERC20(WBTC);
    IERC20Mint private usdt = IERC20Mint(USDT);

    MarketAdapterUniswapV3 private uniAdapter = new MarketAdapterUniswapV3(
    	'UniV3',
    	ISwapRouter(UNI_SWAP_ROUTERV3),
    	WETH,
        UNI_QUOTER
    );

    function setUp() public {}

    function test_DeployedAdress() public {
        console.log("MarketAdapterUniswapV3:", address(uniAdapter));
        assertEq(address(uniAdapter.SWAP_ROUTERV3()), UNI_SWAP_ROUTERV3);
    }

    function testWETH() public {
        (bool success, bytes memory r)  = address(UNI_SWAP_ROUTERV3).call(
            abi.encodeWithSignature(
                "WETH9()"
            )
        );
        console.log("weth:", success);
    }

    function test_swapExactNativeInToERC20Out() public {
        assertEq(uniAdapter.name(), 'UniV3');
        //console.log(address(uni.SWAP_ROUTERV3()));
        assertEq(usdt.balanceOf(address(uniAdapter)), 0); 
        
        // uint deadline
        address[] memory path = new address[](2);
        path[0] = WETH;
        path[1] = USDT;
        uint256 estimateAmount = uniAdapter.getAmountOut(1e18, path);
        uint amountOut = uniAdapter.swapExactNativeInToERC20Out{value: 1e18}(
            1e18, 
            0, 
            path,
            address(uniAdapter), 
            block.timestamp
        );
        uint256 usdtFromFirstSwap = usdt.balanceOf(address(uniAdapter));
        console.log("usdt.balanceOf(uniAdapter):", usdtFromFirstSwap);
        assertEq(estimateAmount, amountOut);

        // SWAP USDT to BTC
        path[0] = USDT;
        path[1] = WBTC;
        estimateAmount = uniAdapter.getAmountOut(usdtFromFirstSwap, path);
        amountOut = uniAdapter.swapExactERC20InToERC20Out(
            usdtFromFirstSwap, 
            0, 
            path,
            address(uniAdapter), 
            block.timestamp
        );
        uint256 wbtcFromSecondSwap = wbtc.balanceOf(address(uniAdapter));
        assertEq(estimateAmount, amountOut);

        // SWAP BTC to ETH
        path[0] = WBTC;
        path[1] = WETH;
        estimateAmount = uniAdapter.getAmountOut(wbtcFromSecondSwap, path);
        amountOut = uniAdapter.swapExactERC20InToNativeOut(
            wbtcFromSecondSwap, 
            0, 
            path,
            address(uniAdapter), 
            block.timestamp
        );
        assertEq(estimateAmount, amountOut);

    }

    /*function test_slippage() public {
        
        address[] memory path = new address[](2);
        path[0] = WETH;
        path[1] = USDT;
        uint256 estimateAmount = uniAdapter.getAmountOut(1e18, path);
        vm.expectRevert("Too little received");
        uint amountOut = uniAdapter.swapExactNativeInToERC20Out{value: 1e18}(
            1e18, 
            1e18, 
            path,
            address(uniAdapter), 
            block.timestamp
        );
    }

    function test_getAmountOut() public {
        address[] memory path = new address[](2);
        path[0] = WETH;
        path[1] = USDT;

        uint256 amountOut;
        amountOut = uniAdapter.getAmountOut(0, path);
        assertEq(amountOut, 0);
    }*/

    // function test_swapExactERC20InToERC20Out() public {
    //     vm.prank(usdt.owner());
    //     usdt.issue(USDT_MINT_AMOUNT);
    //     vm.prank(usdt.owner());
    //     //usdt.transfer(address(uni), USDT_MINT_AMOUNT);
    //     //console.log(msg.sender);
    //     //assertEq(usdt.balanceOf(address(uni)), USDT_MINT_AMOUNT); 
    //     usdt.approve(address(uni), USDT_MINT_AMOUNT);
    //     console.log("next=usdt.balanceOf(this):", usdt.balanceOf(address(this)));

    // }
}
