// SPDX-License-Identifier: MIT
// MarketAdapterCustomMarket for like UniSwapV2 market 
pragma solidity 0.8.21;

import '@uniswap/v3-periphery/contracts/interfaces/ISwapRouter.sol';
import '@uniswap/v3-periphery/contracts/interfaces/IQuoter.sol';
import '@uniswap/v3-periphery/contracts/libraries/TransferHelper.sol';
import "@openzeppelin/contracts/access/Ownable.sol";
//import '../interfaces/IUniversalRouter.sol';
import "../interfaces/IMarketAdapter.sol";
import "../interfaces/IOracleAdapter.sol";

/// @dev Adapter for  UniswapV3
/// @dev All assets should be transfered to this contract balance 
/// @dev before call. Native asset should be in msg.value
contract MarketAdapterUniswapV3 is IMarketAdapter, IOracleAdapter {

    string public name;
    // address immutable public ROUTERV3;
    ISwapRouter public immutable SWAP_ROUTERV3;
    address immutable public QUOTER_ROUTERV3;
    address immutable public WETH;

    // For this example, we will set the pool fee to 0.3%.
    uint24 public  poolFee = 3000;
    uint160 public sqrtPriceLimitX96;


    event ReceivedEther(address, uint);

    constructor(
        string memory _name, 
        ISwapRouter _routerV3, 
        address _weth, 
        address _quoter
    )
    {
        
        require(_weth != address(0) && address(_routerV3) != address(0), 'No Zero address');
        name = _name;
        SWAP_ROUTERV3 = _routerV3;
        WETH = _weth;
        QUOTER_ROUTERV3 = _quoter;

    }

    receive() external payable {
        emit ReceivedEther(msg.sender, msg.value);
    }

    function swapExactNativeInToERC20Out(
        uint256 amountIn,
        uint256 amountOutMin,
        address[] memory path,
        address recipient,
        uint deadline
    ) external payable returns (uint256 amountOut){

        amountOut = _encodeAndSwapUniV3(
            amountIn,
            amountOutMin,
            path,
            recipient,
            deadline
        );
    }


    function swapExactERC20InToERC20Out(
        uint256 amountIn,
        uint256 amountOutMin,
        address[] memory path,
        address recipient,
        uint deadline
    ) external returns (uint256 amountOut){
        TransferHelper.safeApprove(path[0], address(SWAP_ROUTERV3), amountIn);
        amountOut = _encodeAndSwapUniV3(
            amountIn,
            amountOutMin,
            path,
            recipient,
            deadline
        );
    }

    function swapExactERC20InToNativeOut(
        uint256 amountIn,
        uint256 amountOutMin,
        address[] memory path,
        address recipient,
        uint deadline
    ) external returns (uint256 amountOut){
        TransferHelper.safeApprove(path[0], address(SWAP_ROUTERV3), amountIn);
        amountOut = _encodeAndSwapUniV3(
            amountIn,
            amountOutMin,
            path,
            recipient,
            deadline
        );
    }

    function swapERC20InToExactNativeOut(
        uint256 amountInMax,
        uint256 amountOut,
        address[] memory path,
        address recipient,
        uint deadline
    ) external returns (uint256 amountIn){}

    function swapNativeInToExactERC20Out(
        uint256 amountInMax,
        uint256 amountOut,
        address[] memory path,
        address recipient,
        uint deadline
    ) external payable returns (uint256 amountIn){}

    function swapERC20InToExactERC20Out(
        uint256 amountInMax,
        uint256 amountOut,
        address[] memory path,
        address recipient,
        uint deadline
    ) external returns (uint256 amountIn){}

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    ///                       Oracle Adpater Featrures                     ///
    //////////////////////////////////////////////////////////////////////////
    function getAmountOut(uint amountIn,  address[] memory path ) 
        external 
        //view 
        returns (uint256 amountOut)
    {
        //uint256[] memory amts = new uint256[](path.length);
        //bytes memory pathBytes = abi.encodePacked(path[0], DEFAULT_POOL_FEE, path[1]);
        // amountOut = IQouter(QUOTER_ROUTERV3).function quoteExactInput(
        //     pathBytes, 
        //     amountIn
        // );
        if (amountIn != 0) {
            amountOut = IQuoter(QUOTER_ROUTERV3).quoteExactInputSingle(
                path[0],
                path[1],
                poolFee,
                amountIn,
                sqrtPriceLimitX96
            ); 
        }
    }

    function getAmountIn(uint amountOut, address[] memory path)
        external
        view
        returns (uint256 amountIn)
    {}
    /////////////////////////////////////////////////////////////////////////
    function _encodeAndSwapUniV3(
        uint256 amountIn,
        uint256 amountOutMin,
        address[] memory path,
        address recipient,
        uint deadline
    ) internal returns(uint256 amountOut){
        ISwapRouter.ExactInputSingleParams memory params =
            ISwapRouter.ExactInputSingleParams({
                tokenIn: path[0],
                tokenOut: path[path.length - 1],
                fee: poolFee,
                recipient:recipient,
                deadline: deadline,
                amountIn: amountIn,
                amountOutMinimum: amountOutMin,
                sqrtPriceLimitX96: 0
            });

        // The call to `exactInputSingle` executes the swap.
        if (msg.value > 0) {
                amountOut = SWAP_ROUTERV3.exactInputSingle{value: amountIn}(params);
            } else {
                amountOut = SWAP_ROUTERV3.exactInputSingle(params);
            }
    }

    

}