pragma solidity ^0.8.13;

import "forge-std/Script.sol";
import "../contracts/MarketAdapterUniswapV3.sol";

address constant WETH = 0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2;
address constant UNI_SWAP_ROUTERV3 = 0xE592427A0AEce92De3Edee1F18E0157C05861564;
address constant UNI_QUOTER = 0xb27308f9F90D607463bb33eA1BeBb41C27CE5AB6;
contract DeployScript is Script {
    
    function run() external {
        uint256 deployerPrivateKey = vm.envUint("PRIVATE_KEY");
        vm.startBroadcast(deployerPrivateKey);

        MarketAdapterUniswapV3 adapter = new MarketAdapterUniswapV3(
            'UniV3',
            ISwapRouter(UNI_SWAP_ROUTERV3),
            WETH,
            UNI_QUOTER
        );
        vm.stopBroadcast();
    }
}