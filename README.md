# UBD UniswapV3 Market Adapter
## Usage
### Clone with submodules
```bash
git clone git@gitlab.com:ubd2/ubd-market-adapters.git
git submodule update --init --recursive
```
### Build

```shell
$ forge build
```

### Test

```shell
forge test
# or forked Ethereum Mainnet
source .env
forge test -vvvv --fork-url  https://mainnet.infura.io/v3/$WEB3_INFURA_PROJECT_ID --etherscan-api-key $ETHERSCAN_TOKEN
```

### Format

```shell
$ forge fmt
```

### Gas Snapshots

```shell
$ forge snapshot
```

### Help

```shell
$ forge --help
$ anvil --help
$ cast --help
```
